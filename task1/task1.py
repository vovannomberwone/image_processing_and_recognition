import sys
from PIL import Image
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import cv2 as cv
from skimage.filters import sobel, threshold_otsu
from skimage.measure import label
from scipy.ndimage import center_of_mass, binary_fill_holes
matplotlib.rcParams["figure.dpi"] = 800


def main():
	filename = sys.argv[1]
	img = Image.open(filename)
	img_edg = sobel(np.asarray(img))
	img_edg = (img_edg * 255).astype(np.uint8)
	img_bl = Image.fromarray(img_edg, mode="RGB").convert("L")
	img_bl = np.asarray(img_bl)
	img_norm = (img_bl - img_bl.min()) / (img_bl.max() - img_bl.min())
	thresh = threshold_otsu(img_norm)
	img_bin = img_norm > thresh
	img_fill = binary_fill_holes(img_bin)
	img_fill = np.asarray(img_fill).astype(np.uint8)
	kernel_erode = cv.getStructuringElement(cv.MORPH_ELLIPSE, (6, 6))
	img_splited = cv.cv2.erode(img_fill, kernel_erode, iterations=2)
	labels, num_labels = label(img_splited, background=0, return_num=True)
	X = []
	Y = []
	for l in range(1, num_labels + 1):
	    mask = labels == l
	    M = center_of_mass(mask * img_fill)
	    Y.append(M[0])
	    X.append(M[1])
	img_black = Image.fromarray(np.zeros(np.asarray(img).shape), mode="RGB")
	mask = (labels > 0).astype(np.uint8)
	mask *= 255
	mask = Image.fromarray(mask, mode="L")
	img_selected = Image.composite(img, img_black, mask)
	num_points = {(x, y): [0, 0, 0] for x, y in zip(X, Y)}
	with open(filename[:-4] + "_info.txt", "w") as f:
		f.write(f"{num_labels}\n")
		f.write("\n".join((f"{x}, {y}; {', '.join(map(str, num_points[(x, y)]))}" 
			for x, y in zip(X, Y))))



if __name__ == '__main__':
	main()
