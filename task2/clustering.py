from draw_lines import prepare_features
import os
import cv2 as cv
import numpy as np
import pandas as pd
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score


def nearest_dist(lines, i):
    return min([np.linalg.norm(lines[i][1] - lines[j][1]) for j in range(len(lines)) if
                i != j])


def nearest_ind(lines, v):
    return np.array([np.linalg.norm(v - lines[j][1]) for j in range(len(lines))]).argmin()


def order_lines(lines):
    i = np.array([nearest_dist(lines, i) for i in range(len(lines))]).argmax()
    lines = lines[::]
    ordered_lines = [lines[i]]
    cur_l = lines.pop(i)
    while len(lines):
        i = nearest_ind(lines, cur_l[1])
        ordered_lines.append(lines[i])
        cur_l = lines.pop(i)
    return ordered_lines


def get_feature(img):
    c_h, c_w, local_maxs, local_mins, lines = prepare_features(img)
    ordered_lines = order_lines(lines)
    feature = np.array([[np.linalg.norm(l[1] - l[0]), np.linalg.norm(l[1] - l[2])] for l in
                        ordered_lines]).flatten()
    return feature


def clustering():
    features, labels = [], []
    for p in os.scandir("training"):
        labels.append(p.name)
    if os.path.exists("features.npy"):
        X = np.load("features.npy")
    else:
        for p in os.scandir("training"):
            try:
                img = cv.imread(p.path)
                features.append(get_feature(img))
            except Exception as e:
                print(e)
        X = np.vstack(features)
        np.save("features", X)

    dists = ((X[:, np.newaxis, :] - X[np.newaxis, :, :]) ** 2).sum(axis=2) ** 0.5
    three_nearest = dists.argsort(axis=1)[:, 1:4]
    d = {"object": [], "nearests": []}
    for i in range(len(labels)):
        d["object"].append(labels[i])
        d["nearests"].append(';'.join([labels[j] for j in three_nearest[i]]))
    pd.DataFrame.from_dict(d).to_csv('./nearests.csv')

    n_clusters = 18
    kmeans_setting = KMeans(n_clusters=n_clusters, random_state=10)
    cluster_labels = kmeans_setting.fit_predict(X)

    d = {'person': [], 'images': []}
    for i in range(n_clusters):
        d['person'].append(i + 1)
        cluster_names = np.array(labels)[cluster_labels == i].tolist()
        d['images'].append(';'.join(cluster_names))
    pd.DataFrame.from_dict(d).to_csv('./clustering.csv')


if __name__ == "__main__":
    clustering()
